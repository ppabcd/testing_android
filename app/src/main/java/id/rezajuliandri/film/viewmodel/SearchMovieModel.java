package id.rezajuliandri.film.viewmodel;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;
import id.rezajuliandri.film.BuildConfig;
import id.rezajuliandri.film.data.Movie;

public class SearchMovieModel extends ViewModel {
    private static String API_KEY = BuildConfig.API_KEY;
    private MutableLiveData<ArrayList<Movie>> listMovies = new MutableLiveData<>();


    public void setMovie(final String search, final String type) {
        AsyncHttpClient client = new AsyncHttpClient();
        final ArrayList<Movie> listItems = new ArrayList<>();
        String lang;
        Log.d("Language", Locale.getDefault().getLanguage());
        if (Locale.getDefault().getLanguage() == "in") {
            lang = "id-ID";
        } else {
            lang = "en-US";
        }
        String url = "https://api.themoviedb.org/3/search/" + type + "?api_key=" + API_KEY + "&query=" + search + "&language=" + lang;
        Log.d("URLDATA", url);
        client.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    String result = new String(responseBody);
                    JSONObject responseObject = new JSONObject(result);
                    JSONArray list = responseObject.getJSONArray("results");

                    for (int i = 0; i < list.length(); i++) {
                        JSONObject movie = list.getJSONObject(i);
                        Movie movieItems = new Movie(movie, type);
                        listItems.add(movieItems);
                    }
                    listMovies.postValue(listItems);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.d("onFailure", error.getMessage());
            }
        });
    }

    public LiveData<ArrayList<Movie>> getMovies() {
        return listMovies;
    }
}