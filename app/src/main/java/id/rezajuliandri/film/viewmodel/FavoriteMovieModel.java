package id.rezajuliandri.film.viewmodel;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.ArrayList;

import id.rezajuliandri.film.data.Movie;
import id.rezajuliandri.film.db.DatabaseContract;

import static id.rezajuliandri.film.db.DatabaseContract.FavoriteColumns.CONTENT_URI;

public class FavoriteMovieModel extends ViewModel {
    private MutableLiveData<ArrayList<Movie>> listMovies = new MutableLiveData<>();
    Uri uridata;

    public void setMovie(final String type, ContentResolver resolver) {
        try {
            final ArrayList<Movie> listItems = new ArrayList<>();
            uridata = Uri.parse(CONTENT_URI + "/");
            String dataId = type;
            Cursor res = resolver.query(
                    uridata
                    , null
                    , DatabaseContract.FavoriteColumns.TYPE + "= ?"
                    , new String[]{type}
                    , null);

            StringBuffer buffer = new StringBuffer();
            while (res.moveToNext()) {
                Movie movieItem = new Movie(res);
                listItems.add(movieItem);
            }
            listMovies.postValue(listItems);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public LiveData<ArrayList<Movie>> getMovies() {
        return listMovies;
    }
}
