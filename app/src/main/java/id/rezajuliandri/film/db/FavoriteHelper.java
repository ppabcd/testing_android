package id.rezajuliandri.film.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import static id.rezajuliandri.film.db.DatabaseContract.FavoriteColumns.TABLE_NAME;
import static id.rezajuliandri.film.db.DatabaseContract.FavoriteColumns;

public class FavoriteHelper {
    private static final String DATABASE_TABLE = TABLE_NAME;

    private static DatabaseHelper databaseHelper;
    private static FavoriteHelper INSTANCE;

    private static SQLiteDatabase database;

    private FavoriteHelper(Context context) {
        databaseHelper = new DatabaseHelper(context);
    }

    public static FavoriteHelper getInstance(Context context) {
        if (INSTANCE == null) {
            synchronized (SQLiteOpenHelper.class) {
                if (INSTANCE == null) {
                    INSTANCE = new FavoriteHelper(context);
                }
            }
        }
        return INSTANCE;
    }

    public void open() throws SQLException {
        database = databaseHelper.getWritableDatabase();
    }

    public void close() {
        databaseHelper.close();
        if (database.isOpen())
            database.close();
    }

    // Queryyy

    public int countData(String id) {

        Cursor getData = database.rawQuery("SELECT count(*) FROM " + TABLE_NAME + " WHERE " + FavoriteColumns._ID + "='" + id + "'", null);
        getData.moveToFirst();
        int count = getData.getInt(0);
        getData.close();
        return count;
    }

    public int deleteData(String id) {
        return database.delete(TABLE_NAME, FavoriteColumns._ID + "= '" + id + "'", null);
    }

    public long insert(ContentValues values) {
        return database.insert(DATABASE_TABLE, null, values);
    }

    public boolean insertData(String id, String title, String image, String description, String release_date, String vote_average, String type) {

        Cursor getData = database.rawQuery("SELECT count(*) FROM " + TABLE_NAME + " WHERE " + FavoriteColumns._ID + "='" + id + "'", null);
        getData.moveToFirst();
        int count = getData.getInt(0);
        getData.close();
        Log.d("COUNTING", Integer.toString(count));
        if (count != 0) {
            return false;
        }

        ContentValues contentValues = new ContentValues();
        contentValues.put(FavoriteColumns._ID, id);
        contentValues.put(FavoriteColumns.TITLE, title);
        contentValues.put(FavoriteColumns.IMAGE, image);
        contentValues.put(FavoriteColumns.DESCRIPTION, description);
        contentValues.put(FavoriteColumns.RELEASE_DATE, release_date);
        contentValues.put(FavoriteColumns.VOTE_AVERAGE, vote_average);
        contentValues.put(FavoriteColumns.TYPE, type);
        long result = database.insert(TABLE_NAME, null, contentValues);
        if (result == -1)
            return false;
        else
            return true;
    }

    public Cursor getAllData() {
        Cursor res = database.rawQuery("Select * from " + TABLE_NAME, null);
        return res;
    }

    public Cursor getAllDataByType(String type) {
        Log.d("CURSOR", type);
        Cursor res = database.rawQuery("Select * from " + TABLE_NAME + " WHERE type = '" + type + "'", null);
        return res;
    }
}
