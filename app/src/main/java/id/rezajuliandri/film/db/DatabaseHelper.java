package id.rezajuliandri.film.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import static id.rezajuliandri.film.db.DatabaseContract.FavoriteColumns;
import static id.rezajuliandri.film.db.DatabaseContract.FavoriteColumns.TABLE_NAME;

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "Favorite.db";
    private static final String SQL_Favorite_CREATE = String.format("CREATE table %s ( %s INTEGER PRIMARY KEY, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT)",
            TABLE_NAME,
            FavoriteColumns._ID,
            FavoriteColumns.TITLE,
            FavoriteColumns.IMAGE,
            FavoriteColumns.DESCRIPTION,
            FavoriteColumns.RELEASE_DATE,
            FavoriteColumns.VOTE_AVERAGE,
            FavoriteColumns.TYPE);


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
        SQLiteDatabase db = this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(SQL_Favorite_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }
}
