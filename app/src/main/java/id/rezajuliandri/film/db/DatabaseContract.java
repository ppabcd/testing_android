package id.rezajuliandri.film.db;

import android.net.Uri;
import android.provider.BaseColumns;

public class DatabaseContract {
    public static final String AUTHORITY = "id.rezajuliandri.film";
    private static final String SCHEME = "content";

    public static final class FavoriteColumns implements BaseColumns{
        public static final String TABLE_NAME = "Favorite_table";

        // Favorite title
        public static final String TITLE = "title";
        // Favotite image
        public static final String IMAGE = "image";
        // Favorite description
        public static final String DESCRIPTION = "description";
        // Favorite release_date
        public static final String RELEASE_DATE = "release_date";
        // Favorite vote_average
        public static final String VOTE_AVERAGE = "vote_average";
        // Favorite type
        public static final String TYPE = "type";

        public static final Uri CONTENT_URI = new Uri.Builder().scheme(SCHEME)
                .authority(AUTHORITY)
                .appendPath(TABLE_NAME)
                .build();
    }
}
