package id.rezajuliandri.film.adapter;

import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.Locale;

import id.rezajuliandri.film.activity.DetailActivity;
import id.rezajuliandri.film.data.Movie;
import id.rezajuliandri.film.R;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ListViewHolder> {
    private ArrayList<Movie> listMovie;

    // Set data list movie
    public void setData(ArrayList<Movie> items) {
        listMovie.clear();
        listMovie.addAll(items);
        notifyDataSetChanged();

    }

    public ListAdapter(ArrayList<Movie> list) {
        this.listMovie = list;
    }

    @NonNull
    @Override
    public ListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row, parent, false);
        return new ListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ListViewHolder holder, final int position) {
        final Movie movie = listMovie.get(position);
        Log.d("ImageP", "https://image.tmdb.org/t/p/original" + movie.getImage());
        Glide.with(holder.itemView.getContext())
                .load("https://image.tmdb.org/t/p/original" + movie.getImage())
                .apply(new RequestOptions().override(100, 150))
                .into(holder.img);
        holder.tvTitle.setText(movie.getTitle());
        holder.tvDescription.setText(movie.getDescription());

        if (holder.tvDescription.getText().toString().trim().isEmpty()) {
            if (Locale.getDefault().getLanguage() == "in") {
                holder.tvDescription.setText("Tidak ada ikhtisar tersedia");
            } else {
                holder.tvDescription.setText("There is no overview available");
            }
        }
        holder.tvScore.setText(movie.getScore());
        holder.tvDate.setText(movie.getDate());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), DetailActivity.class);
                intent.putExtra(DetailActivity.EXTRA_DATA, listMovie.get(position));
                view.getContext().startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return listMovie.size();
    }

    public class ListViewHolder extends RecyclerView.ViewHolder {
        ImageView img;
        TextView tvTitle, tvDescription, tvScore, tvDate;

        public ListViewHolder(@NonNull View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.img_film);
            tvTitle = itemView.findViewById(R.id.title_film);
            tvDescription = itemView.findViewById(R.id.description_film);
            tvScore = itemView.findViewById(R.id.score_film);
            tvDate = itemView.findViewById(R.id.date_film);
        }
    }
}
