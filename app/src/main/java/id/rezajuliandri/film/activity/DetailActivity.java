package id.rezajuliandri.film.activity;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.Locale;

import id.rezajuliandri.film.R;
import id.rezajuliandri.film.data.Movie;
import id.rezajuliandri.film.db.DatabaseContract;

import static id.rezajuliandri.film.db.DatabaseContract.FavoriteColumns.CONTENT_URI;

public class DetailActivity extends AppCompatActivity {
    private static Movie movie;
    public static final String EXTRA_DATA = "extra_data";
    FloatingActionButton fab;
    ImageView image;
    TextView titleFilm, descriptionFilm, dateFilm, scoreFilm, type, id;
    Boolean isFavorite;
    private Uri uridata;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Movie movie = getIntent().getParcelableExtra(EXTRA_DATA);
        image = findViewById(R.id.image);
        titleFilm = findViewById(R.id.title_film);
        descriptionFilm = findViewById(R.id.description_film);
        dateFilm = findViewById(R.id.date_film);
        scoreFilm = findViewById(R.id.score_film);
        type = findViewById(R.id.type);
        id = findViewById(R.id.id);
        fab = findViewById(R.id.fab);
        Glide.with(this)
                .load("https://image.tmdb.org/t/p/original" + movie.getImage())
                .apply(new RequestOptions().override(500, 750))
                .into(image);
        titleFilm.setText(movie.getTitle());
        descriptionFilm.setText(movie.getDescription());
        if (descriptionFilm.getText().toString().trim().isEmpty()) {
            if (Locale.getDefault().getLanguage() == "in") {
                descriptionFilm.setText("Tidak ada ikhtisar tersedia");
            } else {
                descriptionFilm.setText("There is no overview available");
            }
        }
        dateFilm.setText(movie.getDate());
        scoreFilm.setText(movie.getScore());
        setTitle(movie.getTitle());
        id.setText(movie.getId());
        type.setText(movie.getType());
        addFavorite(movie);
        checkFavorite();
    }

    private void checkFavorite() {
        uridata = Uri.parse(CONTENT_URI + "/" + id.getText().toString());
        String dataId = id.getText().toString();
        Cursor cursor = getContentResolver().query(
                uridata
                , null
                , DatabaseContract.FavoriteColumns._ID + "= ?"
                , new String[]{dataId}
                , null);

        if (cursor.moveToFirst()) {
            isFavorite = true;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                fab.setImageDrawable(getResources().getDrawable(R.drawable.ic_star_white_24dp, getBaseContext().getTheme()));
            } else {
                fab.setImageDrawable(getResources().getDrawable(R.drawable.ic_star_white_24dp));
            }
        } else {
            isFavorite = false;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                fab.setImageDrawable(getResources().getDrawable(R.drawable.ic_star_border_white_24dp, getBaseContext().getTheme()));
            } else {
                fab.setImageDrawable(getResources().getDrawable(R.drawable.ic_star_border_white_24dp));
            }
        }
    }

    private void addFavorite(final Movie movie) {
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isFavorite) {
                    uridata = Uri.parse(CONTENT_URI + "/" + id.getText().toString());
                    String dataId = id.getText().toString();
                    Cursor cursor = getContentResolver().query(uridata, null, DatabaseContract.FavoriteColumns._ID + "= ?", new String[]{dataId}, null);
                    if (!cursor.moveToFirst()) {
                        ContentValues value = new ContentValues();
                        value.put(DatabaseContract.FavoriteColumns._ID, id.getText().toString());
                        value.put(DatabaseContract.FavoriteColumns.TITLE, titleFilm.getText().toString());
                        value.put(DatabaseContract.FavoriteColumns.IMAGE, movie.getImage());
                        value.put(DatabaseContract.FavoriteColumns.DESCRIPTION, descriptionFilm.getText().toString());
                        value.put(DatabaseContract.FavoriteColumns.RELEASE_DATE, dateFilm.getText().toString());
                        value.put(DatabaseContract.FavoriteColumns.VOTE_AVERAGE, scoreFilm.getText().toString());
                        value.put(DatabaseContract.FavoriteColumns.TYPE, type.getText().toString());
                        getContentResolver().insert(CONTENT_URI, value);
                    }
                    Toast.makeText(DetailActivity.this, "Data Inserted", Toast.LENGTH_SHORT).show();
                    checkFavorite();
                } else {
                    uridata = Uri.parse(CONTENT_URI + "/" + id.getText().toString());
                    getContentResolver().delete(uridata, null, null);
                    Toast.makeText(DetailActivity.this, "Data deleted", Toast.LENGTH_SHORT).show();
                    checkFavorite();
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
