package id.rezajuliandri.film.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;

import id.rezajuliandri.film.R;
import id.rezajuliandri.film.adapter.ListAdapter;
import id.rezajuliandri.film.data.Movie;
import id.rezajuliandri.film.helpers.MenuHelpers;
import id.rezajuliandri.film.viewmodel.SearchMovieModel;

public class SearchActivity extends AppCompatActivity {
    public static final String EXTRA_QUERY = "extra_query";
    public static final String EXTRA_TYPE = "extra_type";
    SearchMovieModel searchMovieModel;
    private RecyclerView rvMovies;
    private TextView errorMessages;
    private ArrayList<Movie> list = new ArrayList<>();
    private String title = "Movie";
    private String dataType;
    private String type;
    ProgressBar progressBar;
    ListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        Bundle extras = getIntent().getExtras();
        String query = extras.getString(EXTRA_QUERY);
        type = extras.getString(EXTRA_TYPE);

        Log.d("TYPEEE", type);
        Log.d("TYPEE", Boolean.toString(type == "tv"));
        if (type.equals("tv")) {
            if (Locale.getDefault().getLanguage() == "in") {
                dataType = "Acara TV";
            } else {
                dataType = "TV Show";
            }
        } else {
            if (Locale.getDefault().getLanguage() == "in") {
                dataType = "Film";
            } else {
                dataType = "Movie";
            }
        }

        if (Locale.getDefault().getLanguage() == "in") {
            title = "Hasil Pencarian (" + dataType + ") " + query;
        } else {
            title = "Search Result (" + dataType + ") " + query;
        }
        setTitle(title);

        searchMovieModel = ViewModelProviders.of(this).get(SearchMovieModel.class);
        searchMovieModel.getMovies().observe(this, getMovies);
        // Inflate the layout for this fragment
        rvMovies = findViewById(R.id.contentData);
        progressBar = findViewById(R.id.progressBar);
        errorMessages = findViewById(R.id.message_error);
        showLoading(true);

        showRecyclerList();
        searchMovieModel.setMovie(query, "movie");
    }

    private void showLoading(boolean show) {
        if (show == true) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }

    private Observer<ArrayList<Movie>> getMovies = new Observer<ArrayList<Movie>>() {
        @Override
        public void onChanged(ArrayList<Movie> movies) {
            if (movies != null) {
                adapter.setData(movies);
                showLoading(false);
            }
            if (adapter.getItemCount() == 0) {
                errorMessages.setText("Tidak ada data yang tersedia");
                errorMessages.setVisibility(View.VISIBLE);
                errorMessages.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            }
        }
    };

    private void showRecyclerList() {
        rvMovies.setLayoutManager(new LinearLayoutManager(this));
        adapter = new ListAdapter(list);
        rvMovies.setAdapter(adapter);
    }
}
