package id.rezajuliandri.film.services;

import android.content.Intent;
import android.widget.RemoteViewsService;

import id.rezajuliandri.film.provider.WidgetDataProvider;

public class WidgetService extends RemoteViewsService {

    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {
        return new WidgetDataProvider(this);
    }
}
