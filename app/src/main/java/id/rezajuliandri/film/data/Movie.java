package id.rezajuliandri.film.data;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import org.json.JSONObject;

import java.util.Locale;

public class Movie implements Parcelable {

    private String title;
    private String description;
    private String date;
    private String score;
    private String id;
    private String type;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    private String image;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.description);
        dest.writeString(this.date);
        dest.writeString(this.score);
        dest.writeString(this.image);
        dest.writeString(this.id);
        dest.writeString(this.type);
    }

    public Movie(Cursor data) {
        StringBuffer buffer = new StringBuffer();
        this.id = data.getString(0);
        this.title = data.getString(1);
        this.image = data.getString(2);
        this.description = data.getString(3);
        this.date = data.getString(4);
        this.score = data.getString(5);
    }

    public Movie(JSONObject object, String type) {
        try {
            if (type == "movie") {
                this.title = object.getString("title");
                this.date = object.getString("release_date");
                this.type = "movie";

            } else {
                this.title = object.getString("name");
                this.date = object.getString("first_air_date");
                this.type = "tv";
            }
            this.description = object.getString("overview");
            Log.d("Overview", object.getString("overview"));
            if (object.isNull("overview")) {
                if (Locale.getDefault().getLanguage() == "in") {
                    this.description = "Tidak ada overview tersedia";
                } else {
                    this.description = "There is no overview available";
                }
            }
            Log.d("JSON", object.getString("poster_path"));
            this.image = object.getString("poster_path");
            Double vote = object.getDouble("vote_average");
            this.score = Double.toString(vote);
            this.id = object.getString("id");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected Movie(Parcel in) {
        this.title = in.readString();
        this.description = in.readString();
        this.date = in.readString();
        this.score = in.readString();
        this.image = in.readString();
        this.id = in.readString();
        this.type = in.readString();
    }

    public static final Parcelable.Creator<Movie> CREATOR = new Parcelable.Creator<Movie>() {
        @Override
        public Movie createFromParcel(Parcel source) {
            return new Movie(source);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };
}
