package id.rezajuliandri.film.helpers;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuInflater;
import android.widget.Toast;

import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.FragmentActivity;

import id.rezajuliandri.film.R;
import id.rezajuliandri.film.activity.DetailActivity;
import id.rezajuliandri.film.activity.SearchActivity;

public class MenuHelpers {
    public static void menu(final Context context, Menu menu, MenuInflater inflater, FragmentActivity activity,final String type){
        SearchManager searchManager = (SearchManager) context.getSystemService(context.SEARCH_SERVICE);
        if (searchManager != null) {
            SearchView searchView = (SearchView) (menu.findItem(R.id.search)).getActionView();
            searchView.setSearchableInfo(searchManager.getSearchableInfo(activity.getComponentName()));
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    Toast.makeText(context, query, Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(context, SearchActivity.class);
                    intent.putExtra(SearchActivity.EXTRA_QUERY, query);
                    intent.putExtra(SearchActivity.EXTRA_TYPE, type);
                    context.startActivity(intent);
                    return true;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    return false;
                }
            });
        }
    }
}
