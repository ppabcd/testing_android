package id.rezajuliandri.film.provider;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import static id.rezajuliandri.film.db.DatabaseContract.FavoriteColumns.CONTENT_URI;
import static id.rezajuliandri.film.db.DatabaseContract.FavoriteColumns.TITLE;

public class WidgetDataProviders implements RemoteViewsService.RemoteViewsFactory {
    private Context context;
    private Cursor mCursor;
    private int widgetId;

    public WidgetDataProviders(Context context, Intent intent){
        this.context = context;
        widgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,
                AppWidgetManager.INVALID_APPWIDGET_ID);
    }
    @Override
    public void onCreate() {

    }

    @Override
    public void onDataSetChanged() {
        if(mCursor != null){
            mCursor.close();
        }
        mCursor = context.getContentResolver().query(CONTENT_URI, null, null,null,null,null);
    }

    @Override
    public void onDestroy() {
        if(mCursor != null){
            mCursor.close();
        }
    }

    @Override
    public int getCount() {
        return mCursor.getCount();
    }

    @Override
    public RemoteViews getViewAt(int i) {
        String title = "Tidak ada data tersedia";
        if(mCursor.moveToPosition(i)){
            final int titleColIndex = mCursor.getColumnIndex(TITLE);
            title = mCursor.getString(titleColIndex);
        }
        RemoteViews views = new RemoteViews(context.getPackageName(), android.R.layout.simple_list_item_1);
        views.setTextViewText(android.R.id.text1, title);
        return views;
    }

    @Override
    public RemoteViews getLoadingView() {
        return null;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }
}
