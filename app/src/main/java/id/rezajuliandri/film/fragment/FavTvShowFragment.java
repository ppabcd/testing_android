package id.rezajuliandri.film.fragment;

import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Locale;

import id.rezajuliandri.film.adapter.ListAdapter;
import id.rezajuliandri.film.data.Movie;
import id.rezajuliandri.film.R;
import id.rezajuliandri.film.viewmodel.FavoriteMovieModel;

public class FavTvShowFragment extends Fragment {
    private RecyclerView rvMovies;
    private TextView errorMessages;
    private ArrayList<Movie> list = new ArrayList<>();
    private FavoriteMovieModel favoriteMovieModel;
    private String title = "Movie";
    ProgressBar progressBar;
    ListAdapter adapter;

    public FavTvShowFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
        favoriteMovieModel.setMovie("tv", getActivity().getContentResolver());
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (Locale.getDefault().getLanguage() == "in") {
            title = "Favorit: Acara TV";
        } else {
            title = "Favorite: TV Show";
        }
        setActionBarTitle(title);
    }

    private void showLoading(boolean show) {
        if (show == true) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        favoriteMovieModel = ViewModelProviders.of(this).get(FavoriteMovieModel.class);
        favoriteMovieModel.getMovies().observe(this, getMovies);
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_main, null);
        rvMovies = v.findViewById(R.id.contentData);
        progressBar = v.findViewById(R.id.progressBar);
        errorMessages = v.findViewById(R.id.message_error);
        showLoading(true);

        showRecyclerList();
        favoriteMovieModel.setMovie("tv", getActivity().getContentResolver());
        return v;
    }

    private Observer<ArrayList<Movie>> getMovies = new Observer<ArrayList<Movie>>() {
        @Override
        public void onChanged(ArrayList<Movie> movies) {
            if (movies != null) {
                adapter.setData(movies);
                showLoading(false);
            }
            if (adapter.getItemCount() == 0) {
                errorMessages.setText("Tidak ada data yang tersedia");
                errorMessages.setVisibility(View.VISIBLE);
                errorMessages.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            }
        }
    };

    private void setActionBarTitle(String title) {
        if (((AppCompatActivity) getActivity()).getSupportActionBar() != null) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);
        }
    }

    private void showRecyclerList() {
        rvMovies.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        adapter = new ListAdapter(list);
        rvMovies.setAdapter(adapter);
    }
}
