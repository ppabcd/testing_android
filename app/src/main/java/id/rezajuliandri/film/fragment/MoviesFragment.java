package id.rezajuliandri.film.fragment;


import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Locale;

import id.rezajuliandri.film.adapter.ListAdapter;
import id.rezajuliandri.film.data.Movie;
import id.rezajuliandri.film.R;
import id.rezajuliandri.film.helpers.MenuHelpers;
import id.rezajuliandri.film.viewmodel.MainMovieModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class MoviesFragment extends Fragment {
    private RecyclerView rvMovies;
    private ArrayList<Movie> list = new ArrayList<>();
    private MainMovieModel mainMovieModel;
    private String title = "Movie";
    ProgressBar progressBar;
    ListAdapter adapter;

    public MoviesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        MenuHelpers.menu(getContext(), menu, inflater, getActivity(), "movie");
    }
    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (Locale.getDefault().getLanguage() == "in") {
            title = "Film";
        } else {
            title = "Movie";
        }
        setActionBarTitle(title);
    }

    private void showLoading(boolean show) {
        if (show == true) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mainMovieModel = ViewModelProviders.of(this).get(MainMovieModel.class);
        mainMovieModel.getMovies().observe(this, getMovies);
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_main, null);
        rvMovies = v.findViewById(R.id.contentData);
        progressBar = v.findViewById(R.id.progressBar);
        showLoading(true);

        showRecyclerList();
        mainMovieModel.setMovie("movie");
        return v;
    }

    private Observer<ArrayList<Movie>> getMovies = new Observer<ArrayList<Movie>>() {
        @Override
        public void onChanged(ArrayList<Movie> movies) {
            if (movies != null) {
                adapter.setData(movies);
                showLoading(false);
            }
        }
    };

    private void setActionBarTitle(String title) {
        if (((AppCompatActivity) getActivity()).getSupportActionBar() != null) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);
        }
    }

    private void showRecyclerList() {
        rvMovies.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        adapter = new ListAdapter(list);
        rvMovies.setAdapter(adapter);
    }
}
